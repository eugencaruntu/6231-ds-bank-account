Your task in this lab is to start using the workflow we have seen in the labs to make the tests pass  

This is a maven project, you can run the tests locally using the command  
`mvn test`  

Start by cloning _your repo_ locally, make the required changes, then push your final code to your repo  

To clone the repo  
`git clone <REPO_URL>`  
This will create a local copy of your repo to make changes and modification  

When you modify the code, you can add yoiu modifications using  
`git add <FILENAME>` to add only changes made to <FILENAME>, or  
`git add .` to add all changes  

To commit you changes  
`git commit -m <MESSAGE>`  

To push your changes to the repo    
`git push origin HEAD`  


Please note that you should NOT make changes to the tests to make them pass  
The tests are meant to be a guide for you on what to implement  
All changes to the tests will be disgarded when calculating the grade of the assignment  

To build the code on travis, go to `https://travis-ci.com/dashboard`  
Look for you the entry corresponding to your repo under `Active Repositories`  
Then click `Trigger a build`  