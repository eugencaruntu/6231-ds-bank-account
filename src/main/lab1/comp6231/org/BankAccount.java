package lab1.comp6231.org;

public abstract class BankAccount {

    protected int accNum;
    protected double balance;
    protected static int serialNum = 0;

    /**
     * Default constructor
     */
    public BankAccount() {
        accNum = ++serialNum;
        balance = 0.0;
    }

    /**
     * Overloaded constructor with a specified start balance
     *
     * @param startBalance the initial balance
     * @throws Exception when startBalance is negative
     */
    public BankAccount(double startBalance) throws Exception {
        if (startBalance >= 0) {
            accNum = serialNum++;
            balance = startBalance;
        } else {
            throw new Exception("Negative balance");
        }
    }

    /**
     * Accessor for balance
     *
     * @return the account balance
     */
    public double getBalance() {
        return balance;
    }

    /**
     * Accessor for account number
     *
     * @return the account number
     */
    public int getAccNum() {
        return accNum;
    }

    /**
     * Deposit amount of money, if it is legal/valid amount
     *
     * @param amount the amount to be deposited
     * @throws Exception when the amount to deposit is negative
     */
    public void deposit(double amount) throws Exception {
        if (amount > 0) {
            balance += amount;
        } else {
            throw new Exception("Deposit must be positive");
        }
    }

    /**
     * Withdraw amount from account
     *
     * @param amount the amount to be withdrawn
     * @throws Exception when amount is negative or greater than the balance
     */
    public void withdraw(double amount) throws Exception {
        if (amount > balance)
            throw new Exception("Insufficient balance. Transaction not executed.");
        else if (amount <= 0.0)
            throw new Exception("Amount must be greater than zero. Transaction not executed.");
        else
            balance -= amount;
    }

    /**
     * Override toString()
     *
     * @return the balance amount for specific account type displaying the account number.
     */
    public String toString() {
        return "The balance is of the " + accType() + " " + accNum + " is " + balance;
    }

    /**
     * Apply a computation on the account
     */
    public abstract void applyComputation();

    /**
     * Obtain the account type
     *
     * @return a String value for the account type
     */
    public abstract String accType();
}